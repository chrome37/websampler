var bpmToSec = function (bpm) {
    var sec = 60 / bpm * 4;
    return sec;
}

var countTosSec = function (sec, csec) {
    return sec + csec / 100;
}