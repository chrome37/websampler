$(function () {
    $("#grid").attr("width", $("#grid").css("width"));
    $("#back").attr("width", $("#back").css("width"));

    var canvas = document.getElementById("grid");
    var ctx = canvas.getContext("2d");

    var back = document.getElementById("back");
    var ctxBack = back.getContext("2d");


    var w = canvas.width;
    var h = canvas.height;
    var x = 0;
    var dt = 10;//msec
    var T = $("#barInput").val() * bpmToSec($("#bpmInput").val());
    var NT = T / (dt / 1000);
    var dx = w / NT;

    var loop;

    var dTempo = bpmToSec($("#bpmInput").val()) / 4;
    var NG = T / dTempo;
    var dxTempo = w / NG;

    ctxBack.strokeStyle = "#ddd";
    for (var i = 0; i < T / dTempo; i++) {
        ctxBack.beginPath();
        ctxBack.moveTo(x, 0);
        ctxBack.lineTo(x, h);
        ctxBack.stroke();
        x = x + dxTempo;
    }

    ctx.strokeStyle = "#d9534f";
    ctx.lineWidth = 2
    $("#play").on("click", function (e) {
        clearInterval(loop);
        loop = setInterval(function () {
            ctx.clearRect(0, 0, w, h);
            ctx.beginPath();
            ctx.moveTo(x, 0);
            ctx.lineTo(x, h);
            ctx.stroke();

            if (x > w) {
                x = 0;
            } else {
                x = x + dx;
            }
        }, 10);
    });

    $("#pause").on("click", function (e) {
        clearInterval(loop);
    });

    $("#stop").on("click", function (e) {
        clearInterval(loop);
        x = 0;
        ctx.clearRect(0, 0, w, h);
    });

    $("#bpmInput").on("change", function (e) {
        $("#stop").trigger("click");
        T = $("#barInput").val() * bpmToSec($("#bpmInput").val());
        NT = T / (dt / 1000);
        dx = w / NT;

        dTempo = bpmToSec($("#bpmInput").val()) / 4;
        NG = T / dTempo;
        dxTempo = w / NG;

        for (var i = 0; i < T / dTempo; i++) {
            ctxBack.beginPath();
            ctxBack.moveTo(x, 0);
            ctxBack.lineTo(x, h);
            ctxBack.stroke();
            x = x + dxTempo;
        }
    });

    $("#barInput").on("change", function (e) {
        $("#stop").trigger("click");
        T = $("#barInput").val() * bpmToSec($("#bpmInput").val());
        NT = T / (dt / 1000);
        dx = w / NT;

        dTempo = bpmToSec($("#bpmInput").val()) / 4;
        NG = T / dTempo;
        dxTempo = w / NG;

        for (var i = 0; i < T / dTempo; i++) {
            ctxBack.beginPath();
            ctxBack.moveTo(x, 0);
            ctxBack.lineTo(x, h);
            ctxBack.stroke();
            x = x + dxTempo;
        }
    })
});
