$(function () {

    var count = [0, 0, 0];

    var counter;
    var p1, p2


    p1 = '0' + String(count[1]).slice(-2);
    p2 = '0' + String(count[0]).slice(-2);

    $("#bar").html(count[2]);
    $("#sec").html(count[1]);
    $("#csec").html(count[0]);


    $("#play").on("click", function (e) {
        clearInterval(counter);
        counter = setInterval(function (t) {
            p1 = '0' + String(count[1]).slice(-2);
            p2 = '0' + String(count[0]).slice(-2);

            $("#bar").html(count[2]);
            $("#sec").html(count[1]);
            $("#csec").html(count[0]);

            count[0]++;
            if (count[0] == 100) {
                count[0] = 0;
                count[1]++;
            }
            if (countTosSec(count[1], count[0]) == bpmToSec($("#bpmInput").val())) {
                count[0] = 0;
                count[1] = 0;
                count[2]++;
            }
            if (count[2] == $("#barInput").val()) {
                count[0] = 0;
                count[1] = 0;
                count[2] = 0;
            }
        }, 10);
    });

    $("#pause").on("click", function (e) {
        clearInterval(counter);
    });

    $("#stop").on("click", function (e) {
        clearInterval(counter);
        count[0] = 0;
        count[1] = 0;
        count[2] = 0;
        $("#bar").html(count[2]);
        $("#sec").html(count[1]);
        $("#csec").html(count[0]);
    });
});